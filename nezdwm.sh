#!/bin/bash

# dir init
rm -rf dwm dwm_bin dwm_dl_patches
mkdir dwm_bin dwm_dl_patches

# checkout
git clone https://git.suckless.org/dwm
cd dwm
git checkout "$(< ../config/commit.txt)"

# download & apply all patches
wget -i ../config/patches.txt -P../dwm_dl_patches
find ../patches ../dwm_dl_patches -type f -name "*.diff" | sort | while read p; do
  echo "applying patch $p"
  patch < "$p"
done

# copy config.h
cp -v ../config/config.h .

# compile
make -j4
make DESTDIR="$(pwd)/../dwm_bin" PREFIX="" install -j4

# erase evidence >:)
cd ..
rm -rf dwm dwm_dl_patches
